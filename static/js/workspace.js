const MessageTypeError = "error";
const MessageTypeSuccess = "success";

function ajax(url, method, data, responseType, complete) {
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (xhttp.status !== 200 || this.readyState !== 4) {
            return;
        }

        let res = xhttp.responseText;
        if (responseType === "json" && res !== "") {
            res = JSON.parse(res);
        }

        complete(res);
    };
    xhttp.open(method, url, false);
    xhttp.send(data);
}

function serializeForm(form) {
    let formData = {};

    for (let elem of form.elements) {
        if (!elem.name || !elem.name.length || elem.name in formData) {
            continue;
        }

        formData[elem.name] = elem.value;
    }

    return formData
}

function addMessage(type, message) {
    let elem = document.createElement("label");
    elem.className = "notify notify-" + type;
    elem.innerText = message;

    document.getElementById("message-list").appendChild(elem);
}

function handleDefaultResponse(resp) {
    if (resp.redirect) {
        location.href = resp.redirect;
    }

    if (resp && resp.messages && resp.messages.length) {
        for (let message of resp.messages) {
            if (!message || !message.type) {
                continue;
            }

            switch (message.type) {
                case MessageTypeError:
                    addMessage(MessageTypeError, message.message);
                    break;
                case MessageTypeSuccess:
                    addMessage(MessageTypeSuccess, message.message);
                    break;
            }
        }
    }
}