function onDefaultHandleForm(element) {
    ajax(element.action, element.method, JSON.stringify(serializeForm(element)), "json", function(resp) {
        handleDefaultResponse(resp);
    });
}
