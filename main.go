package main

import (
	"log"
	"net/http"
	"github.com/gorilla/mux"
	"./interfaces/config"
	"./infrastructure/database"
	"./infrastructure/utils"
	mainMailer "./interfaces/email"
	mainResponse "./interfaces/response"
	mainHttp "./interfaces/http"

	userHttp "./app/user/delivery/http"
	authHttp "./app/auth/delivery/http"
	userRepo "./app/user/repository"
	userUC "./app/user/usecase"

	"time"
	"os"
	"flag"
	"runtime"
)

var (
	confPath = flag.String("config", "./config/app.json", "path to config json file")
)

func init()  {
	// Use all cpu
	runtime.GOMAXPROCS(runtime.NumCPU())

	flag.Parse()

	// Initialize config data from json ile
	config.Initialize(*confPath)

	// Initialize global response handlers (HTML, JSON)
	mainResponse.InitGlobalResponse(config.GlobalConfig.TemplateLayoutPath, config.GlobalConfig.TemplateIncludePath)

	// Initialize global session storage
	mainHttp.InitGlobalSession(config.GlobalConfig.SessionStorage, config.GlobalConfig.StoreSessionKey, config.GlobalConfig.SessionExpire)

	// Connection to DB
	if con, err := database.CreateConnection(config.GlobalConfig.Database); err != nil {
		panic(err)
	} else {
		database.SetConnection(con)
	}

	// Run mail sender channel listener
	go mainMailer.Run(config.GlobalConfig.Email)
}

func main()  {
	startServer()
}

// Start http server
func startServer()  {
	router := mux.NewRouter()

	uuc := userUC.NewUserUseCase(userRepo.NewMysqlUserRepository(database.SQL))
	utuc := userUC.NewUserTokensUseCase(userRepo.NewMysqlUserTokensRepository(database.SQL))

	hd := authHttp.AuthHandler{
		UUsecase: uuc,
		UTokenUsecase: utuc,
		OAuthState: utils.UUID(),
		OAuthConfig: config.GetOAuthConfig(
			config.GlobalConfig.GoogleAuth.ClientID,
			config.GlobalConfig.GoogleAuth.ClientSecret,
			config.GlobalConfig.OAuthRedirect,
		),
	}

	authHttp.NewAuthHTTPHandler(router, hd)
	userHttp.NewUserHTTPHandler(router, uuc)

	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./static/"))))

	srv := &http.Server{
		Handler:      router,
		Addr:         ":" + os.Getenv("LISTEN_PORT"),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}
