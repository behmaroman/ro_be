package utils

import (
	"os"
	"fmt"
)

func IsDirectory(path string) (bool, error)  {
	info, err := os.Stat(path)
	if err != nil {
		return false, err
	}
	if !info.IsDir() {
		return false, fmt.Errorf("utron: %s is not a directory", path)
	}

	return true, nil
}
