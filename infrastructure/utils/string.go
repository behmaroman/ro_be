package utils

import (
	"encoding/base64"
	"crypto/rand"
	"io"
	"github.com/google/uuid"
	"fmt"
	"crypto/md5"
)

const (
	EmailRegexp  = `^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`
)

func SID() string  {
	b := make([]byte, 32)
	if _, err := io.ReadFull(rand.Reader, b); err != nil {
		return ""
	}
	return base64.URLEncoding.EncodeToString(b)
}

func UUID() string  {
	return uuid.New().String()
}

func MD5(st string) string  {
	return fmt.Sprintf("%x", md5.Sum([]byte(st)))
}

func Base64Encode(str string) string {
	return base64.StdEncoding.EncodeToString([]byte(str))
}

func Base64Decode(str string) string {
	decoded, err := base64.StdEncoding.DecodeString(str)
	if err != nil {
		return ""
	}
	return string(decoded)
}