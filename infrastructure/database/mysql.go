package database

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql" // MySQL driver
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"database/sql"
)

type MySQLInfo struct {
	Username 	string	`json:"username"`
	Password 	string	`json:"password"`
	Name 		string	`json:"name"`
	Hostname 	string	`json:"hostname"`
	Port 		int		`json:"port"`
	Parameter 	string	`json:"parameter"`
}

func DSN(ci MySQLInfo) string {
	return ci.Username +
		":" +
		ci.Password +
		"@tcp(" +
		ci.Hostname +
		":" +
		fmt.Sprintf("%d", ci.Port) +
		")/" +
		ci.Name + ci.Parameter
}

func MySQLConnect(d MySQLInfo) (res *sql.DB, err error) {
	res, err = sql.Open("mysql", DSN(d))
	return res, err
}
