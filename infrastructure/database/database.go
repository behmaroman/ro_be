package database

import (
	"database/sql"
)

const (
	TypeMySQL Type = "MySQL"
)

var (
	SQL *sql.DB
)

type (
	Type string

	DataBase struct {
		DBType Type			`json:"DBType"`
		MySQL  MySQLInfo	`json:"MySQL"`
	}

	CRUD interface {
		Create()
	}
)

func CreateConnection(d DataBase) (res *sql.DB, err error) {
	switch d.DBType {
	case TypeMySQL:
		res, err = MySQLConnect(d.MySQL)
	}

	return res, err
}

func SetConnection(sql *sql.DB)  {
	SQL = sql
}