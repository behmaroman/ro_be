package json

import (
	"io"
	"encoding/json"
)

type JsonRender struct {}

func (jr *JsonRender) Render(out io.Writer, name string, data interface{}) error {
	res, err := json.Marshal(data)
	if err != nil {
		return err
	}

	_, err = out.Write(res)
	return err
}
