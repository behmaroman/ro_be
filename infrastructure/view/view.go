package view

import (
	"io"
)

type View interface {
	Render(out io.Writer, name string, data interface{}) error
}