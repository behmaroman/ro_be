package html

import (
	"fmt"
	"github.com/oxtoacart/bpool"
	"../../view"
	"../../utils"
	"html/template"
)

func NewHTMLRender(layouts string, include string) (view.View, error) {
	_, err := utils.IsDirectory(layouts)
	if err != nil {
		return nil, fmt.Errorf("%s is not a directory", layouts)
	}

	_, err = utils.IsDirectory(layouts)
	if err != nil {
		return nil, fmt.Errorf("%s is not a directory", include)
	}

	s := &HTMLRender{
		templateLayoutPath: layouts,
		templateIncludePath: include,
		bufpool: bpool.NewBufferPool(64),
		templates: make(map[string]*template.Template),
	}

	return s.load()
}
