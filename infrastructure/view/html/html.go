package html

import (
	"github.com/oxtoacart/bpool"
	"path/filepath"
	"os"
	"strings"
	"io"
	"fmt"
	"html/template"
	"../../view"
)

var mainTmpl = `{{define "main" }} {{ template "base" . }} {{ end }}`

type HTMLRender struct {
	templateLayoutPath  string
	templateIncludePath string
	bufpool *bpool.BufferPool
	templates map[string]*template.Template
	renderData interface{}
}

func (s *HTMLRender) load() (view.View, error) {
	layoutFiles, err := filepath.Glob(s.templateLayoutPath + "*.gohtml")
	if err != nil {
		return nil, err
	}

	mainTemplate := template.New("main")

	mainTemplate, err = mainTemplate.Parse(mainTmpl)
	if err != nil {
		return nil, err
	}

	supported := map[string]bool{".gohtml": true}

	werr := filepath.Walk(s.templateIncludePath, func(path string, f os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if f.IsDir() {
			return nil
		}

		extension := filepath.Ext(path)
		if _, ok := supported[extension]; !ok {
			return nil
		}

		name := path[len(s.templateIncludePath) - 2:]
		name = filepath.ToSlash(name)
		name = strings.TrimPrefix(name, "/") // case  we missed the opening slash
		name = strings.TrimSuffix(name, extension) // remove extension

		files := append(layoutFiles, path)
		s.templates[name], err = mainTemplate.Clone()
		if err != nil {
			return err
		}
		s.templates[name] = template.Must(s.templates[name].ParseFiles(files...))
		return nil
	})

	if werr != nil {
		return nil, werr
	}

	return s, nil
}

func (s *HTMLRender) Render(out io.Writer, name string, data interface{}) error {
	s.renderData = data

	tmpl, ok := s.templates[name]
	if !ok {
		return fmt.Errorf("the template %s does not exist", name)
	}

	buf := s.bufpool.Get()
	defer s.bufpool.Put(buf)

	err := tmpl.Execute(buf, s.renderData)
	if err != nil {
		return err
	}

	buf.WriteTo(out)

	return nil
}
