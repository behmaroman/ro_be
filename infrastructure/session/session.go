package session

const SessionStorageMemory = "providers"

type (
	Provider interface {
		SessionInit(sid string) (Session, error)
		SessionRead(sid string) (Session, error)
		SessionDestroy(sid string) error
		SessionGC(maxLifeTime int64)
	}

	Session interface {
		Set(key, value interface{}) error //set session value
		Get(key interface{}) interface{}  //get session value
		Delete(key interface{}) error     //delete session value
		SessionID() string                //back current sessionID
	}
)

func NewManager(provider Provider, cookieName string, maxlifetime int64) *Manager {
	return &Manager{
		provider: provider,
		cookieName: cookieName,
		maxlifetime: maxlifetime,
	}
}

func StartSession(manager *Manager) *Manager {
	globalSessions := manager
	go globalSessions.GC()

	return globalSessions
}