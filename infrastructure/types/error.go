package types

type Errors struct {
	Errors []string
}

func (e *Errors) AddError(err string)  {
	e.Errors = append(e.Errors, err)
}

func (e *Errors) ClearErrors()  {
	e.Errors = e.Errors[:0]
}

func (e Errors) GetErrors() []string  {
	return e.Errors
}
