package email

import (
	"../../infrastructure/email"
)

var mailChannel = make(chan MailData)

type MailData struct {
	Email string
	Subject string
	Content string
}

func (md MailData) Valid() bool  {
	return md.Email != ""
}

// Send message to channel when send mail
func SendMail(email, subj, cont string)  {
	mailChannel <- MailData{
		Email: email,
		Subject: subj,
		Content: cont,
	}
}

func Run(c email.SMTPInfo) {
	email.Configure(c)

	for {
		if message := <-mailChannel; message.Valid() {
			email.SendEmail(message.Email, message.Subject, message.Content)
		}
	}
}