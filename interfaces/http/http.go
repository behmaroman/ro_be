package http

import (
	"net/http"
	"../../infrastructure/session"
	"../../infrastructure/session/providers"
	model "../../app/user"
	"io/ioutil"
	"encoding/json"
)

const (
	userSessionKey = "user"
)

var (
	GlobalSession *session.Manager

	ContentTypeHTML = "text/html"
	ContentTypeJson = "application/json"
	ContentTypeRedirect = ""
)

// Struct which is parameter for http request handler
type HandlerParams struct {
	Response http.ResponseWriter
	Request *http.Request
	Session session.Session
	User model.User
}

func (rd HandlerParams) GetSessionData(key string) interface{}  {
	return rd.Session.Get(key)
}

func (rd *HandlerParams) SetSessionData(key string, data interface{})  {
	if rd.Session == nil {
		rd.Session = GlobalSession.SessionStart(rd.Response, rd.Request)
	}
	rd.Session.Set(key, data)
}

func (rd *HandlerParams) SetSessionUser(user model.User)  {
	rd.User = user
	rd.SetSessionData(userSessionKey, user)
}

func (rd *HandlerParams) GetSessionUser() model.User  {
	user := rd.GetSessionData(userSessionKey)
	if user == nil {
		return model.User{}
	}
	return user.(model.User)
}

func (rd *HandlerParams) JsonFromRequest(i interface{})  {
	body, err := ioutil.ReadAll(rd.Request.Body)
	if err != nil {
		return
	}

	json.Unmarshal(body, &i)
}

func (rd *HandlerParams) JsonFromResponse(i interface{}, r *http.Response) {
	defer r.Body.Close()
	contents, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return
	}

	json.Unmarshal(contents, &i)
}

func NewHTTPData(w http.ResponseWriter, r *http.Request, s session.Session) HandlerParams  {
	return HandlerParams{
		Response: w,
		Request: r,
		Session: s,
	}
}

func InitGlobalSession(storage, key string, expire int64)  {
	var prov session.Provider
	switch storage {
	case session.SessionStorageMemory:
		prov = providers.GetProvider()
	default:
		prov = providers.GetProvider()
	}

	GlobalSession = session.StartSession(session.NewManager(prov, key, expire))
	if GlobalSession == nil {
		panic("Not initialized session")
	}
}