package http

import (
	"net/http"
)

type (
	MainHandler func(rd HandlerParams)
)

func MainMiddleware(next MainHandler, needAuth bool, contentType string) http.HandlerFunc  {
	return func(w http.ResponseWriter, r *http.Request) {
		rd := NewHTTPData(w, r, GlobalSession.GetSession(w, r))

		w.Header().Set("Content-Type", contentType)

		needAuthF := func() {
			if rd.Session == nil {
				http.Redirect(w, r, "/", http.StatusFound)
				return
			}

			user := rd.GetSessionUser()
			if user.ID == 0 {
				GlobalSession.SessionDestroy(w, r)
				http.Redirect(w, r, "/", http.StatusFound)
				return
			}

			rd.User = user

			next(rd)
		}

		notAuthF := func() {
			if rd.Session == nil {
				next(rd)
				return
			}

			user := rd.GetSessionUser()
			if user.ID == 0 {
				GlobalSession.SessionDestroy(w, r)
				next(rd)
				return
			}

			http.Redirect(w, r, "/profile", http.StatusFound)
		}

		if needAuth {
			needAuthF()
		} else {
			notAuthF()
		}
	}
}