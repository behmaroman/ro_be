package response

import (
	"../../infrastructure/view"
	"io"
)

type RenderData struct {
	Title string
	Session interface{}
	Data interface{}
	Configs interface{}
}

type HTMLResponse struct {
	View string
	RenderData RenderData
	viewHandel *view.View
}

func (ht HTMLResponse) Render(w io.Writer)  {
	(*ht.viewHandel).Render(w, ht.View, ht.RenderData)
}

