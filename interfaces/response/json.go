package response

import (
	"../../infrastructure/view"
	"io"
)

const (
	MessageTypeSuccess = "success"
	MessageTypeError = "error"
)

type (
	Type string

	JsonResponse struct {
		Success bool				`json:"success"`
		Messages []ResponseMessage	`json:"messages"`
		Redirect string				`json:"redirect"`
		viewHandel *view.View
	}

	ResponseMessage struct {
		Type Type		`json:"type"`
		Message string	`json:"message"`
	}
)

func (jr *JsonResponse) AddError(error string)  {
	jr.Messages = append(jr.Messages, ResponseMessage{
		Type: MessageTypeError,
		Message: error,
	})
}

func (jr *JsonResponse) SetErrors(errors []string)  {
	for _, err := range errors {
		jr.AddError(err)
	}
}

func (jr *JsonResponse) AddSuccess(message string)  {
	jr.Messages = append(jr.Messages, ResponseMessage{
		Type: MessageTypeSuccess,
		Message: message,
	})
}

func (jr *JsonResponse) HasErrors() bool  {
	return len(jr.Messages) > 0
}

func (jr JsonResponse) Render(w io.Writer) {
	(*jr.viewHandel).Render(w, "", jr)
}

