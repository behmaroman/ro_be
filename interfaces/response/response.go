package response

import (
	"io"
	"../../infrastructure/view"
	"../../infrastructure/view/html"
	"../../infrastructure/view/json"
)

var (
	GlobalHTMLResponse view.View
	GlobalJsonResponse view.View
)

type IResponse interface {
	Render(w io.Writer)
}

func NewJsonResponse() JsonResponse  {
	return JsonResponse{
		Success: false,
		viewHandel: &GlobalJsonResponse,
	}
}

func NewHTMLResponse(view, title string, data interface{}, session bool) HTMLResponse  {
	resp := HTMLResponse{
		View: view,
		viewHandel: &GlobalHTMLResponse,
	}
	resp.RenderData.Title = title
	resp.RenderData.Session = session
	resp.RenderData.Data = data
	return resp
}

func InitGlobalResponse(layout, include string)  {
	if v, err := html.NewHTMLRender(layout, include); err != nil || v == nil {
		panic("Not initialized view handler")
	} else {
		GlobalHTMLResponse = v
	}

	GlobalJsonResponse = json.NewJsonRender()
}