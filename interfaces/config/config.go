package config

import (
	"../../infrastructure/database"
	"../../infrastructure/email"
	"fmt"
	"os"
	"encoding/json"
	"io/ioutil"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

var GlobalConfig Config

type Config struct {
	StoreSessionKey string  		`json:"StoreSessionKey"`
	SessionExpire int64				`json:"SessionExpire"`
	TemplateLayoutPath string		`json:"TemplateLayoutPath"`
	TemplateIncludePath string		`json:"TemplateIncludePath"`
	SessionStorage string			`json:"SessionStorage"`
	Database database.DataBase		`json:"Database"`
	GoogleAuth GoogleAuth			`json:"GoogleAuth"`
	Email email.SMTPInfo			`json:"Email"`
	OAuthRedirect string
	ResetPassRedirect string
}

type GoogleAuth struct {
	ClientID 		string		`json:"client_id"`
	ProjectID 		string		`json:"project_id"`
	AuthURI 		string		`json:"auth_uri"`
	TokenURI 		string		`json:"token_uri"`
	AuthProvider 	string		`json:"auth_provider_x509_cert_url"`
	ClientSecret 	string		`json:"client_secret"`
	RedirectURIs 	[]string	`json:"redirect_uris"`
}

func Initialize(confPath string)  {
	raw, err := ioutil.ReadFile(confPath)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	json.Unmarshal(raw, &GlobalConfig)

	if &GlobalConfig == nil {
		panic("Config not be loaded")
	}

	GlobalConfig.OAuthRedirect = os.Getenv("REDIRECT_TO_HOST") + os.Getenv("OAUTH2_CALLBACK")
	GlobalConfig.ResetPassRedirect = os.Getenv("REDIRECT_TO_HOST") + os.Getenv("RESET_PASSWORD_URI")
}

func GetOAuthConfig(clientID, clientSecret, redirectURL string) *oauth2.Config  {
	return &oauth2.Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		RedirectURL:  redirectURL,
		Scopes:       []string{"email", "profile"},
		Endpoint:     google.Endpoint,
	}
}