# Roman_Behma

##Configs:

### Sys ENV:

- OAUTH2_CALLBACK - postfix for oauth callback route (*/google/callback*);
- RESET_PASSWORD_URI - postfix for set new password (*/pass/reset*);
- REDIRECT_TO_HOST - host for redirecting (*http://127.0.0.1:8000*)
- LISTEN_PORT - listening port

### JSON. Must be located in /config/app.json

Structure:
```json
{
  "StoreSessionKey": "SID",
  "SessionExpire": 86400,
  "TemplateLayoutPath": "./templates/layouts/",
  "TemplateIncludePath": "./templates/",
  "SessionStorage": "memory",
  "Database": {
    "DBType": "MySQL",
    "MySQL": {
      "username": "root",
      "password": "111",
      "name": "app",
      "hostname": "127.0.0.1",
      "port": 3306,
      "parameter": ""
    }
  },
  "Email": {
    "username": "...",
    "password": "...",
    "hostname": "...",
    "port": 587,
    "from": "..."
  },
  "GoogleAuth": {
    "client_id": "...",
    "project_id": "...",
    "auth_uri": "...",
    "token_uri": "...",
    "auth_provider_x509_cert_url": "...",
    "client_secret": "...",
    "redirect_uris": ["..."]
  }
}
```

##TODO:
0. Two-step registration: first is email and password, second is other required data
1. Session store in Redis
2. Single page application with JSON format data exchange by REST API
3. Add more validators and errors exceptions
4. Perfect split logic into 4 main layers: domain (business entities), usecases (logic for app and business), interfaces (application data handlers), infrastructure (drivers, wrappers etc)
5. Perfect rendering HTML content
6. Create tests