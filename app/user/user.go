package user

import (
	"encoding/json"
	"regexp"
	"strings"
)

const (
	EmailRegexp  = `^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`
)

type User struct {
	ID 			int		`json:"id"`
	FullName 	string	`json:"fullname"`
	Email 		string	`json:"email"`
	Address 	string	`json:"address"`
	Telephone 	string	`json:"telephone"`
	Password 	string	`json:"password"`
}

func (u User) String() string {
	data, err := json.Marshal(u)
	if err != nil {
		return ""
	}

	return string(data)
}

func (u User) Valid() bool {
	if m, _ := regexp.MatchString(EmailRegexp, u.Email); !m {
		return false
	}

	if strings.TrimSpace(u.FullName) == "" {
		return false
	}

	return true
}