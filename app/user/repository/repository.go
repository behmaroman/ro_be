package repository

import (
	"../../user"
)

type UserRepository interface {
	GetByEmail(email string) (*user.User, error)
	GetByToken(token string) (*user.User, error)
	GetByID(id int) (*user.User, error)
	Save(u *user.User) (int64, error)
}

type UserTokensRepository interface {
	GetActiveTokenByUserID(id int) (*user.UserTokens, error)
	DeactivateToken(token string) (bool, error)
	Save(u *user.UserTokens) (int64, error)
}

