package repository

import (
	"database/sql"
	model "../../user"
)

type mysqlUserTokensRepository struct {
	Conn *sql.DB
}

func NewMysqlUserTokensRepository(Conn *sql.DB) UserTokensRepository {
	return &mysqlUserTokensRepository{
		Conn: Conn,
	}
}

func (m *mysqlUserTokensRepository) fetch(query string, args ...interface{}) ([]*model.UserTokens, error) {
	rows, err := m.Conn.Query(query, args...)

	if err != nil {
		return nil, err
	}
	defer rows.Close()
	result := make([]*model.UserTokens, 0)
	for rows.Next() {
		t := new(model.UserTokens)
		err = rows.Scan(
			&t.ID,
			&t.UserID,
			&t.Token,
			&t.Type,
			&t.IsActive,
		)

		if err != nil {
			return nil, err
		}

		result = append(result, t)
	}

	return result, nil
}

func (m *mysqlUserTokensRepository) GetActiveTokenByUserID(id int) (*model.UserTokens, error) {
	query := `SELECT * FROM user_tokens WHERE is_active = 1 AND user_id = ?`

	list, err := m.fetch(query, id)
	if err != nil {
		return nil, err
	}

	a := &model.UserTokens{}
	if len(list) > 0 {
		a = list[0]
	} else {
		return nil, model.NotFoundError
	}
	return a, nil
}

func (m *mysqlUserTokensRepository) DeactivateToken(token string) (bool, error) {
	query := `UPDATE user_tokens
 		      SET is_active = 0
			  WHERE ut.token = ?`

	stmt, err := m.Conn.Prepare(query)
	if err != nil {
		return false, err
	}

	_, err = stmt.Exec(token)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (m *mysqlUserTokensRepository) Save(a *model.UserTokens) (int64, error) {
	query := `INSERT INTO user_tokens (user_id, token, type, is_active) VALUES (?, ?, ?, ?)`
	stmt, err := m.Conn.Prepare(query)
	if err != nil {
		return 0, err
	}

	res, err := stmt.Exec(a.UserID, a.Token, a.Type, a.IsActive)
	if err != nil {
		return 0, err
	}
	return res.LastInsertId()
}
