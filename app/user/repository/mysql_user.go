package repository

import (
	"database/sql"
	model "../../user"
)

type mysqlUserRepository struct {
	Conn *sql.DB
}

func NewMysqlUserRepository(Conn *sql.DB) UserRepository {
	return &mysqlUserRepository{
		Conn: Conn,
	}
}

func (m *mysqlUserRepository) fetch(query string, args ...interface{}) ([]*model.User, error) {
	rows, err := m.Conn.Query(query, args...)

	if err != nil {
		return nil, err
	}
	defer rows.Close()
	result := make([]*model.User, 0)
	for rows.Next() {
		t := new(model.User)
		err = rows.Scan(
			&t.ID,
			&t.FullName,
			&t.Email,
			&t.Address,
			&t.Telephone,
			&t.Password,
		)

		if err != nil {
			return nil, err
		}

		result = append(result, t)
	}

	return result, nil
}

func (m *mysqlUserRepository) GetByEmail(email string) (*model.User, error) {
	query := `SELECT * FROM users WHERE email = ?`

	list, err := m.fetch(query, email)
	if err != nil {
		return nil, err
	}

	a := &model.User{}
	if len(list) > 0 {
		a = list[0]
	} else {
		return nil, model.NotFoundError
	}
	return a, nil
}


func (m *mysqlUserRepository) GetByToken(token string) (*model.User, error) {
	query := `SELECT u.* 
			  FROM users AS u
			  JOIN user_tokens AS ut ON ut.user_id=u.id
			  WHERE ut.token = ?`

	list, err := m.fetch(query, token)
	if err != nil {
		return nil, err
	}

	a := &model.User{}
	if len(list) > 0 {
		a = list[0]
	} else {
		return nil, model.NotFoundError
	}
	return a, nil
}

func (m *mysqlUserRepository) GetByID(id int) (*model.User, error) {
	query := `SELECT * FROM users WHERE id = ?`

	list, err := m.fetch(query, id)
	if err != nil {
		return nil, err
	}

	a := &model.User{}
	if len(list) > 0 {
		a = list[0]
	} else {
		return nil, model.NotFoundError
	}
	return a, nil
}

func (m *mysqlUserRepository) Save(a *model.User) (int64, error) {
	query := `INSERT INTO users (full_name, email, address, telephone, password) VALUES (?, ?, ?, ?, ?)
			  ON DUPLICATE KEY UPDATE full_name=VALUES(full_name), address=VALUES(address), telephone=VALUES(telephone), password=VALUES(password)`
	stmt, err := m.Conn.Prepare(query)
	if err != nil {
		return 0, err
	}

	res, err := stmt.Exec(a.FullName, a.Email, a.Address, a.Telephone, a.Password)
	if err != nil {
		return 0, err
	}
	return res.LastInsertId()
}
