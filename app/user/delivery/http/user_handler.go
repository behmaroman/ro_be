package http

import (
	"github.com/gorilla/mux"
	"../../usecase"
	httpMain "../../../../interfaces/http"
	"../../../../interfaces/response"
)

type UserHandler struct {
	UUsecase usecase.UserUseCase
}

// Return page for view profile data
// Response types: html
func (uh *UserHandler) UserGET(rd httpMain.HandlerParams)  {
	response.NewHTMLResponse("profile/view", "View profile", rd.User, true).Render(rd.Response)
}

// Return profile edit form
// Response types: html
func (uh *UserHandler) UserEditGET(rd httpMain.HandlerParams)  {
	response.NewHTMLResponse("profile/edit", "Edit profile", rd.User, true).Render(rd.Response)
}

// Save profile data
// Response types: json
func (uh *UserHandler) UserEditPOST(rd httpMain.HandlerParams)  {
	rp := response.NewJsonResponse()

	if rd.JsonFromRequest(&rd.User); !rd.User.Valid() {
		rp.AddError("User data is invalid")
		rp.Render(rd.Response)
		return
	}

	uh.UUsecase.Save(&rd.User)

	rd.SetSessionUser(rd.User)

	rp.Redirect = "/profile"
	rp.Success = true
	rp.Render(rd.Response)
}

func NewUserHTTPHandler(router *mux.Router, uu usecase.UserUseCase)  {
	handler := &UserHandler{
		UUsecase: uu,
	}

	router.HandleFunc("/profile", httpMain.MainMiddleware(handler.UserGET, true, httpMain.ContentTypeHTML)).Methods("GET")
	router.HandleFunc("/profile/edit", httpMain.MainMiddleware(handler.UserEditGET, true, httpMain.ContentTypeHTML)).Methods("GET")
	router.HandleFunc("/profile/edit", httpMain.MainMiddleware(handler.UserEditPOST, true, httpMain.ContentTypeJson)).Methods("POST")
}