package usecase

import (
	"../repository"
	"../../user"
)

type UserUseCase interface {
	GetByEmail(email string) (*user.User, error)
	GetByToken(token string) (*user.User, error)
	GetByID(id int) (*user.User, error)
	Save(u *user.User) (*user.User, error)
}

type userUseCase struct {
	userRepo repository.UserRepository
}

func NewUserUseCase(a repository.UserRepository) UserUseCase {
	return &userUseCase{
		userRepo: a,
	}
}

func (a *userUseCase) GetByEmail(email string) (*user.User, error) {
	res, err := a.userRepo.GetByEmail(email)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (a *userUseCase) GetByToken(token string) (*user.User, error) {
	res, err := a.userRepo.GetByToken(token)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (a *userUseCase) GetByID(id int) (*user.User, error) {
	res, err := a.userRepo.GetByID(id)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (a *userUseCase) Save(m *user.User) (*user.User, error) {
	id, err := a.userRepo.Save(m)
	if err != nil {
		return nil, err
	}

	m.ID = int(id)
	return m, nil
}