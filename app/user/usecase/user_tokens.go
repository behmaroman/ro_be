package usecase

import (
	"../repository"
	"../../user"
)

type UserTokensUseCase interface {
	GetActiveTokenByUserID(id int) (*user.UserTokens, error)
	DeactivateToken(token string) (bool, error)
	Save(u *user.UserTokens) (*user.UserTokens, error)
}

type userTokensUseCase struct {
	userTokensRepo repository.UserTokensRepository
}

func NewUserTokensUseCase(a repository.UserTokensRepository) UserTokensUseCase {
	return &userTokensUseCase{
		userTokensRepo: a,
	}
}

func (a *userTokensUseCase) GetActiveTokenByUserID(id int) (*user.UserTokens, error) {
	res, err := a.userTokensRepo.GetActiveTokenByUserID(id)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (a *userTokensUseCase) DeactivateToken(token string) (bool, error) {
	return a.userTokensRepo.DeactivateToken(token)
}

func (a *userTokensUseCase) Save(m *user.UserTokens) (*user.UserTokens, error) {
	id, err := a.userTokensRepo.Save(m)
	if err != nil {
		return nil, err
	}

	m.ID = int(id)
	return m, nil
}