package user

import "errors"

var (
	NotFoundError = errors.New("item is not found")
)
