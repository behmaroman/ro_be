package user

type UserTokens struct {
	ID			int		`json:"id"`
	UserID 		int		`json:"user_id"`
	Token 		string	`json:"token"`
	Type 		int		`json:"type"`
	IsActive 	int		`json:"is_active"`
}

func (u *UserTokens) Valid() bool {
	if u.Token == "" {
		return false
	}

	return true
}
