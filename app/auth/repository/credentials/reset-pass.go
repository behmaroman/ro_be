package credentials

import (
	"../../../../infrastructure/types"
)

type ResetPassCredential struct {
	Token 				string		`json:"token"`
	Password 			string		`json:"password"`
	ConfirmPassword 	string		`json:"password_confirm"`
	types.Errors
}

func (c *ResetPassCredential) Valid() bool  {
	c.ClearErrors()

	if c.Token == "" {
		c.AddError("Token is empty")
	}

	if len(c.Password) < 6 {
		c.AddError("Password must be more than 5 symbols")
	}

	if c.Password != c.ConfirmPassword {
		c.AddError("Passwords is different")
	}

	return len(c.GetErrors()) == 0
}

func (c ResetPassCredential) GetEmail() string  {
	return c.Token
}

