package credentials

import (
	"../../../../infrastructure/types"
)

type AuthCredential struct {
	Email 		string		`json:"email"`
	Password 	string		`json:"password"`
	types.Errors
}

func (c AuthCredential) Valid() bool  {
	return c.Email != "" && c.Password != ""
}

func (c AuthCredential) GetEmail() string  {
	return c.Email
}
