package credentials

import (
	"../../../../infrastructure/types"
)

type GoogleOAuthCredential struct {
	ID            string	`json:"id"`
	Email         string	`json:"email"`
	VerifiedEmail string	`json:"verified_email"`
	Name          string	`json:"name"`
	GivenName     string	`json:"given_name"`
	FamilyName    string	`json:"family_name"`
	Link          string	`json:"link"`
	Picture       string	`json:"picture"`
	Gender        string	`json:"gender"`
	Locale        string	`json:"locale"`
	types.Errors
}

func (c *GoogleOAuthCredential) Valid() bool  {
	c.ClearErrors()

	if c == nil || c.ID == "" || c.Email == "" {
		c.AddError("Google profile data is empty")
	}

	return len(c.GetErrors()) == 0
}

func (c GoogleOAuthCredential) GetEmail() string  {
	return c.Email
}
