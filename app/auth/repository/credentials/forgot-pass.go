package credentials

import (
	"../../../../infrastructure/types"
)

type ForgotCredential struct {
	Email 		string		`json:"email"`
	types.Errors
}

func (c ForgotCredential) Valid() bool  {
	return c.Email != ""
}

func (c ForgotCredential) GetEmail() string  {
	return c.Email
}
