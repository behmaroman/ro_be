package credentials

import (
	"regexp"
	"../../../../infrastructure/types"
	"../../../../infrastructure/utils"
)

type SignUpCredential struct {
	Email string			`json:"email"`
	Password string			`json:"password"`
	ConfirmPassword string	`json:"password_confirm"`
	types.Errors
}

func (c *SignUpCredential) Valid() bool  {
	c.ClearErrors()

	if c.Email == "" || c.Password == "" {
		c.AddError("Email or password is empty")
	}

	if len(c.Password) < 6 {
		c.AddError("Password must be more than 5 symbols")
	}

	if c.Password != c.ConfirmPassword {
		c.AddError("Passwords is different")
	}

	if m, _ := regexp.MatchString(utils.EmailRegexp, c.Email); !m {
		c.AddError("Email is invalid")
	}

	return len(c.GetErrors()) == 0
}

func (c SignUpCredential) GetEmail() string  {
	return c.Email
}
