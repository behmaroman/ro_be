package repository

type Credential interface {
	GetEmail() string
	Valid() bool
}
