package http

import (
	"net/http"
	"context"
	"github.com/gorilla/mux"
	"golang.org/x/oauth2"
	userModel "../../../user"
	"../../../user/usecase"
	"../../repository/credentials"
	"../../repository"
	"../../../../infrastructure/utils"
	"../../../../interfaces/config"
	"../../../../interfaces/email"
	"../../../../interfaces/response"
	httpMain "../../../../interfaces/http"
)

type AuthHandler struct {
	UUsecase usecase.UserUseCase
	UTokenUsecase usecase.UserTokensUseCase
	OAuthConfig *oauth2.Config
	OAuthState string
} 

// Get authorization page
// Response types: html
func (au *AuthHandler) AuthGET(rd httpMain.HandlerParams)  {
	response.NewHTMLResponse("login", "Log in", nil, false).Render(rd.Response)
}

// Authorization user by request orm data
// Response types: json
func (au *AuthHandler) AuthPOST(rd httpMain.HandlerParams) {
	res := response.NewJsonResponse()

	cred := &credentials.AuthCredential{}
	if rd.JsonFromRequest(&cred); cred.Valid() {
		user := au.authenticate(cred)
		if &user != nil && user.ID != 0 && utils.MD5(cred.Password) == user.Password {
			rd.SetSessionUser(user)
			res.Success = true
			res.Redirect = "/profile"
		} else {
			res.AddError("User not found or password is invalid")
		}
	} else {
		res.SetErrors(cred.GetErrors())
	}

	res.Render(rd.Response)
}

// Redirect to google's authorization page
// Response types: redirect
func (au *AuthHandler) AuthGoogleGET(rd httpMain.HandlerParams)  {
	http.Redirect(rd.Response, rd.Request, au.OAuthConfig.AuthCodeURL(au.OAuthState), http.StatusTemporaryRedirect)
}

// Handle google authorization data
// If email is exist User will be authorized
// If email doesn't exist user will be registered and authorized
// Response types: redirect
func (au *AuthHandler) CallBackGoogleGET(rd httpMain.HandlerParams)  {
	state := rd.Request.FormValue("state")
	if state != au.OAuthState {
		http.Redirect(rd.Response, rd.Request, "/", http.StatusTemporaryRedirect)
		return
	}

	code := rd.Request.FormValue("code")
	token, err := au.OAuthConfig.Exchange(context.Background(), code)
	if err != nil {
		http.Redirect(rd.Response, rd.Request, "/", http.StatusTemporaryRedirect)
		return
	}

	resp, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + token.AccessToken)
	authData := &credentials.GoogleOAuthCredential{}
	if rd.JsonFromResponse(&authData, resp); authData.Valid() {
		user := au.authenticate(authData)
		if &user != nil && user.ID != 0 {
			rd.SetSessionUser(user)
			http.Redirect(rd.Response, rd.Request, "/profile", http.StatusTemporaryRedirect)
		} else {
			user := userModel.User{
				Email: authData.Email,
				FullName: authData.Name,
			}

			if au.UUsecase.Save(&user); user.ID != 0 {
				rd.SetSessionUser(user)
				http.Redirect(rd.Response, rd.Request, "/profile", http.StatusTemporaryRedirect)
			} else {
				http.Redirect(rd.Response, rd.Request, "/", http.StatusTemporaryRedirect)
			}
		}
	} else {
		http.Redirect(rd.Response, rd.Request, "/", http.StatusTemporaryRedirect)
		return
	}
}

// Destroy user's session
// Response types: redirect
func (au *AuthHandler) LogoutPOST(rd httpMain.HandlerParams)  {
	httpMain.GlobalSession.SessionDestroy(rd.Response, rd.Request)
	http.Redirect(rd.Response, rd.Request, "/", http.StatusFound)
}

// Handle request form and register new user
// Response types: json
func (au *AuthHandler) SignUpPOST(rd httpMain.HandlerParams)  {
	res := response.NewJsonResponse()

	cred := &credentials.SignUpCredential{}
	if rd.JsonFromRequest(&cred); cred.Valid() {
		user, _ := au.UUsecase.GetByEmail(cred.Email)
		if user == nil || user.ID == 0 {
			user.Password = utils.MD5(cred.Password)
			user, _ := au.UUsecase.Save(user)
			if user.ID != 0 {
				rd.SetSessionUser(*user)
				res.Redirect = "/profile/edit"
			} else {
				res.AddError("Sign up error")
			}
		} else {
			res.AddError("Email already used")
		}
	} else {
		res.SetErrors(cred.GetErrors())
	}

	res.Render(rd.Response)
}

// Get registration page
// Response types: html
func (au *AuthHandler) SignUpGET(rd httpMain.HandlerParams)  {
	response.NewHTMLResponse("signup", "Sign Up", nil, false).Render(rd.Response)
}

// Get page for send link for new password
// Response types: html
func (au *AuthHandler) ForgotPassGET(rd httpMain.HandlerParams)  {
	response.NewHTMLResponse("forgot_pass", "Forgot", nil, false).Render(rd.Response)
}

// Send link or restore password
// Response types: json
func (au *AuthHandler) ForgotPassPOST(rd httpMain.HandlerParams)  {
	res := response.NewJsonResponse()

	cred := &credentials.ForgotCredential{}
	if rd.JsonFromRequest(&cred); !cred.Valid() {
		res.SetErrors(cred.GetErrors())
		res.Render(rd.Response)
		return
	}

	user := au.authenticate(cred)
	if &user == nil || user.ID == 0 {
		res.AddError("Email not found")
		res.Render(rd.Response)
		return
	}

	token, err := au.UTokenUsecase.GetActiveTokenByUserID(user.ID)
	if err != nil || token == nil || token.ID == 0 {
		res.AddError("Email already sent")
		res.Render(rd.Response)
		return
	}

	token = &userModel.UserTokens{
		IsActive: 1,
		Type: 1,
		Token: utils.Base64Encode(utils.UUID()),
		UserID: user.ID,
	}

	if au.UTokenUsecase.Save(token); token.ID == 0 {
		res.AddError("Save token error")
		res.Render(rd.Response)
		return
	}

	email.SendMail(user.Email, "Reset password", au.resetPassURI(token.Token))

	res.Success = true
	res.AddSuccess("Email has been sent")
	res.Render(rd.Response)
}

// Get page for new password by link from user's email
// Response types: html
func (au *AuthHandler) ResetPassGET(rd httpMain.HandlerParams)  {
	token, key := rd.Request.URL.Query()["token"], ""
	if token != nil && len(token) > 0 {
		key = token[0]
	}
	response.NewHTMLResponse("reset_pass", "New password", key, false).Render(rd.Response)
}

// Store new password
// Response types: json
func (au *AuthHandler) ResetPassPOST(rd httpMain.HandlerParams)  {
	res := response.NewJsonResponse()

	cred := &credentials.ResetPassCredential{}
	if rd.JsonFromRequest(&cred); !cred.Valid() {
		res.SetErrors(cred.GetErrors())
		res.Render(rd.Response)
		return
	}

	user, _ := au.UUsecase.GetByToken(cred.Token)
	if user == nil || user.Email == "" {
		res.AddError("User is undefined or token is invalid")
		res.Render(rd.Response)
		return
	}

	user.Password = utils.MD5(cred.Password)
	au.UUsecase.Save(user)
	if user.Password != utils.MD5(cred.Password) {
		res.AddError("Save new password error")
		res.Render(rd.Response)
		return
	}

	au.UTokenUsecase.DeactivateToken(cred.Token)

	res.Success = true
	res.Redirect = "/login"
	res.Render(rd.Response)
}

// auth by credentials
func (au *AuthHandler) authenticate(c repository.Credential) userModel.User {
	res, _ := au.UUsecase.GetByEmail(c.GetEmail())
	return *res
}

// Get url or reset password
func (au *AuthHandler) resetPassURI(token string) string {
	return config.GlobalConfig.ResetPassRedirect + "?token=" + token
}

func NewAuthHTTPHandler(router *mux.Router, handler AuthHandler)  {
	router.HandleFunc("/", httpMain.MainMiddleware(handler.AuthGET, false, httpMain.ContentTypeHTML)).Methods("GET")
	router.HandleFunc("/login", httpMain.MainMiddleware(handler.AuthGET, false ,httpMain.ContentTypeHTML)).Methods("GET")
	router.HandleFunc("/login", httpMain.MainMiddleware(handler.AuthPOST, false, httpMain.ContentTypeJson)).Methods("POST")

	router.HandleFunc("/google/login", httpMain.MainMiddleware(handler.AuthGoogleGET, false, httpMain.ContentTypeHTML)).Methods("GET")
	router.HandleFunc("/google/callback", httpMain.MainMiddleware(handler.CallBackGoogleGET, false, httpMain.ContentTypeRedirect)).Methods("GET")

	router.HandleFunc("/signup", httpMain.MainMiddleware(handler.SignUpGET, false, httpMain.ContentTypeHTML)).Methods("GET")
	router.HandleFunc("/signup", httpMain.MainMiddleware(handler.SignUpPOST, false, httpMain.ContentTypeJson)).Methods("POST")

	router.HandleFunc("/pass/forgot", httpMain.MainMiddleware(handler.ForgotPassGET, false, httpMain.ContentTypeHTML)).Methods("GET")
	router.HandleFunc("/pass/forgot", httpMain.MainMiddleware(handler.ForgotPassPOST, false, httpMain.ContentTypeJson)).Methods("POST")
	router.HandleFunc("/pass/reset", httpMain.MainMiddleware(handler.ResetPassGET, false, httpMain.ContentTypeHTML)).Methods("GET")
	router.HandleFunc("/pass/reset", httpMain.MainMiddleware(handler.ResetPassPOST, false, httpMain.ContentTypeJson)).Methods("POST")

	router.HandleFunc("/logout", httpMain.MainMiddleware(handler.LogoutPOST, true, httpMain.ContentTypeRedirect)).Methods("POST")
}